# Free Persian Fonts API 

Just a simple API for Free Perian Fonts 

## Current Fonts 

|Font Name | Creator           | 
|:---------: |:-----------------:|
| Vazir      | Saber Rastikerdar | 
| Tanha      | Saber Rastikerdar |
| Vazir Code | Saber Rastikerdar |
| Shabnam    | Saber Rastikerdar | 
| Samim      | Saber Rastikerdar | 
| Sahel      | Saber Rastikerdar | 
| Parastoo   | Saber Rastikerdar |
| Nahid      | Saber Rastikerdar |
| Gandom     | Saber Rastikerdar |