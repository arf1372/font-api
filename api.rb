require 'sinatra' 
#require 'sinatra/cross_origin'

set :bind, '0.0.0.0' 
set :public_folder, 'public' 
set :protection, except: :frame_options

configure do 
    enable :cross_origin 
end  

before do 
    response.headers['Access-Control-Allow-Origin'] = '*'
end 

options "*" do
    response.headers["Allow"] = "GET, PUT, POST, DELETE, OPTIONS"
    response.headers["Access-Control-Allow-Headers"] = "Authorization, Content-Type, Accept, X-User-Email, X-Auth-Token"
    response.headers["Access-Control-Allow-Origin"] = "*"
    200
end


get '/' do 
    'Please use GET /fonts?font=FONTNAME to get your stylesheet'
end 

get '/fonts' do 
    headers({'X-Frame-Options' => 'SAMEORIGIN', 'Timing-Allow-Origin' => '*'})
    #headers["Content-Disposition"] = "inline, filename=#{params[:font]}.css"
    #headers["Access-Control-Allow-Origin"] = "http://localhost"
    #response["Access-Control-Allow-Origin"] = "http://localhost"
    cache_control :private, :max_age => 86400
    content_type "text/css"
    #send_file File.read(File.join('public', 'stylesheets/#{params[:font]}.css'))
    f = File.open("public/stylesheets/#{params[:font]}.css") 
    f.read 
    #redirect "/stylesheets/#{params[:font]}.css"
    #send_file "/fonts/#{params[:font]}/#{params[:font]}.ttf"
    

end 


